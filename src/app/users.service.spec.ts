import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from  '@angular/common/http/testing';

import { UsersService } from './users.service';
import { User } from './users/user.model';

const mockUsers: User[] = [
  {id: 1, name: 'abc', username: 'abcc'} as User,
  {id: 1, name: 'xyz', username: 'xyzz'} as User
];

describe('UsersService', () => {
  let service: UsersService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(UsersService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return users', () => {
    service.getUsers().subscribe(users => {
      expect(users.length).toBe(2);
      expect(users).toEqual(mockUsers);
    });
    const req = httpMock.expectOne(`https://jsonplaceholder.typicode.com/users`);
    expect(req.request.method).toBe('GET');
    req.flush(mockUsers);

  });
});
