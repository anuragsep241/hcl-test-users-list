import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { User } from './users/user.model'

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  apiurl = environment.apiurl;

  constructor(private http: HttpClient) {
  }

  getUsers(): Observable<Array<User>> {
      return this.http.get<any[]>(`${this.apiurl}/users`);
  }
}
