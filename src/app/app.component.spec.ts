import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, async, getTestBed, ComponentFixture } from '@angular/core/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { UsersService } from './users.service';
import { FilterUserPipe } from './users/user-filter.pipe';
import { User } from './users/user.model';
import { UsersComponent } from './users/users.component';

const mockUsers: User[] = [
  {id: 1, name: 'abc', username: 'abcc'} as User,
  {id: 1, name: 'xyz', username: 'xyzz'} as User
];

class MockUsersSerivce {
  getUsers() {
    return of(mockUsers);
  }
}


describe('AppComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    let injector;
    let service: UsersService;
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      declarations: [
        AppComponent,
        UsersComponent,
        FilterUserPipe
      ],
      providers: [
        { provide: UsersService, useClass: MockUsersSerivce }
      ]
    }).compileComponents();
    injector = getTestBed();
    service = injector.get(UsersService);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.debugElement.componentInstance;
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
