import { Pipe, PipeTransform } from '@angular/core';
import { User } from './user.model';
/*
 * Filter the users list based on filter parameters
 * Example usersList | filterUser :field : keyword
*/
@Pipe({name: 'filterUser'})
export class FilterUserPipe implements PipeTransform {
  transform(users: Array<User>, field: string, keyword: string): Array<User> {
    if (keyword == null) {
        return users;
    }

    return users.filter(user => user[field].includes(keyword));
  }
}
