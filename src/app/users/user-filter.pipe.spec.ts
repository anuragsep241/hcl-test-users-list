import { FilterUserPipe } from './user-filter.pipe';
import { User } from './user.model';

const users = [
    {
        name: 'xyz',
        username: 'abc'
    } as User,
    {
        name: 'pqr',
        username: 'ltr'
    } as User
];

describe('FilterUserPipe', () => {

    const pipe = new FilterUserPipe();

    it('filters based on filed and keyword', () => {
        expect(pipe.transform(users, 'name', 'a').length).toBe(0);
        expect(pipe.transform(users, 'name', 'xy').length).toBe(1);
    });
});
