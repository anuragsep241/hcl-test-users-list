import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, getTestBed, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { UsersService } from '../users.service';
import { FilterUserPipe } from './user-filter.pipe';
import { User } from './user.model';

import { UsersComponent } from './users.component';


const mockUsers: User[] = [
  {id: 1, name: 'abc', username: 'abcc'} as User,
  {id: 1, name: 'xyz', username: 'xyzz'} as User
];

class MockUsersSerivce {
  getUsers() {
    return of(mockUsers);
  }
}



describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    let injector;
    let service: UsersService;
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule],
      declarations: [
        UsersComponent,
        FilterUserPipe
      ],
      providers: [
        { provide: UsersService, useClass: MockUsersSerivce }
      ]
    })
    .compileComponents();
    injector = getTestBed();
    service = injector.get(UsersService);
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.debugElement.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
