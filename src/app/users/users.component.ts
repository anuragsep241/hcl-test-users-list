import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { FilterOption } from './filter-option.interface';
import { User } from './user.model';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users$: Observable<Array<User>> = this.userService.getUsers();
  userForm = new FormGroup({
    field: new FormControl('name'),
    keyword: new FormControl()
  });
  options: FilterOption[] = [
    {
      value: 'name',
      text: 'Name'
    },
    {
      value: 'username',
      text: 'User Name'
    },
    {
      value: 'email',
      text: 'Email'
    },
    {
      value: 'phone',
      text: 'Phone'
    },
    {
      value: 'website',
      text: 'Website'
    }
  ];

  constructor(private userService: UsersService) { }

  get field() {
    return this.userForm.controls['field'].value;
  }

  get keyword() {
    return this.userForm.controls['keyword'].value;
  }

  ngOnInit(): void {
  }

}
